// MeshFadeController.js
// Version: 0.0.1
// Event: Lens Initialized
// Description: Fade in and out the 3D meshes using fizzle.

// @input bool fadeEffect = false
// @input string fadeType = "startFadeIn" {"widget":"combobox", "values":[{"label":"Fade In", "value":"startFadeIn"}, {"label":"Fade Out", "value":"startFadeOut"}], "showIf": "fadeEffect"} 
// @input float fadingDuration = 1.0 {"widget":"slider", "min":0.1, "max":5.0, "step":0.01 , "showIf": "fadeEffect"} 
// @input Asset.Material[] fadeMaterials {"showIf": "fadeEffect"}

// @input int videoDelay = 1.0 {"widget":"slider", "min":0, "max":10, "step":1}
// @input int cameraHearthOffset = 100 {"widget":"slider", "min":0, "max":500, "step":1}

// @input float videoDuration = 0.0;

var isFading = false;
var timeStartedFading = 0;

var startingValue = 1;
var endingValue = 0;

var videoDuration = 0.0;
var epsilon = 0.001;

script.api.videoFinished = false;
script.api.videoDelay = script.videoDelay;
script.api.allowHintAfterVideo = false;
script.api.cameraHearthOffset = script.cameraHearthOffset;

function onUpdateEvent(eventData)
{
	
    if(isFading && script.fadeMaterials)
    { 
        var timeSinceStarted = getTime() - timeStartedFading;
        var percentageComplete = timeSinceStarted / script.fadingDuration;
        var transition = lerp(startingValue,endingValue,percentageComplete);
        for(var i = 0; i < script.fadeMaterials.length; i++)
        {
            if (script.fadeMaterials[i]) {
                script.fadeMaterials[i].mainPass.transition =  transition;
            }
        }
        if(percentageComplete >= 1.0)
        {
            isFading = false;
        }
    }
	
	videoDuration += getDeltaTime();
}
var updateEvent = script.createEvent("UpdateEvent");
updateEvent.bind(onUpdateEvent); 



function onCustomDelayedEvent(eventData)
{
	//play the video just once
	if(script.fadeMaterials[0].mainPass.baseTex.control.getStatus() === VideoStatus.Paused)
		script.fadeMaterials[0].mainPass.baseTex.control.resume();
    
	script.fadeMaterials[0].mainPass.baseTex.control.setOnFinish(function(){
		if(Math.abs(videoDuration-(script.videoDuration+script.videoDelay)) < epsilon || videoDuration > script.videoDuration + script.videoDelay)
		{
			script.api.videoFinished = true; 
			script.api.allowHintAfterVideo = true;
			script.api.allowHintEvent.reset(3);
		}
	});
}
var customDelayEvent = script.createEvent("DelayedCallbackEvent");
customDelayEvent.bind(onCustomDelayedEvent);

function onAllowHintEvent(eventData)
{
	script.api.allowHintAfterVideo = false;
}
script.api.allowHintEvent = script.createEvent("DelayedCallbackEvent");
script.api.allowHintEvent.bind(onAllowHintEvent);

//add a delay of 2 seconds to the start of the video
if(script.fadeMaterials[0].mainPass.baseTex.control.getStatus() === VideoStatus.Stopped)
{
    script.fadeMaterials[0].mainPass.baseTex.control.play(1);
	videoDuration = 0.0;
}
script.fadeMaterials[0].mainPass.baseTex.control.setOnReady(function(){
    if(script.fadeMaterials[0].mainPass.baseTex.control.getStatus() === VideoStatus.Playing)
	{
		script.fadeMaterials[0].mainPass.baseTex.control.pause();
		customDelayEvent.reset(script.videoDelay);
	}
});
	
script.api.resetFadeEffect = function()
{
    if(script.fadeMaterials)
    {
        for(var i = 0; i < script.fadeMaterials.length; i++)
        {
            if (script.fadeMaterials[i]) {
                script.fadeMaterials[i].mainPass.transition = 1;
            }
        }
    }
}

script.api.startFade = function()
{
    switch(script.fadeType)
    {
        case "startFadeIn":
        script.api.startFadeIn();
        break;

        case "startFadeOut":
        script.api.startFadeOut();
        break;

    }
}

script.api.startFadeOut = function()
{
    if(script.fadeMaterials)
    {
        for(var i = 0; i < script.fadeMaterials.length; i++)
        {
            if (script.fadeMaterials[i]) {
                script.fadeMaterials[i].mainPass.transition = 0;
            }
        }
        
        if(script.fadeMaterials[0].mainPass.baseTex.control.getStatus() !== VideoStatus.Playing)
        {
            if(script.fadeMaterials[0].mainPass.baseTex.control.getStatus() === VideoStatus.Paused)
                script.fadeMaterials[0].mainPass.baseTex.control.resume();
            if(script.fadeMaterials[0].mainPass.baseTex.control.getStatus() === VideoStatus.Stopped)
			{
				script.fadeMaterials[0].mainPass.baseTex.control.play(1);
				videoDuration = 0.0;
			}
        }
        startingValue = 0;
        endingValue = 1;
        timeStartedFading = getTime();
        isFading = true;
    } 
}

script.api.startFadeIn = function()
{
    if(script.fadeMaterials)
    {
        for(var i = 0; i < script.fadeMaterials.length; i++)
        {
            if (script.fadeMaterials[i]) {
                script.fadeMaterials[i].mainPass.transition = 1;
            }
        }
        
        startingValue = 1;
        endingValue = 0;
        timeStartedFading = getTime();
        isFading = true;
    }   
}


function lerp(a, b, t)
{
    return a + t * (b - a);
}
