// MySceneController.js
// Version: 0.0.1
// Event: Lens Initialized
// Description: Plays Mark in the City scene animation.

// @input Component.AnimationMixer heartsScene1
// @input Component.AnimationMixer heartsScene2

//@input Component.ScriptComponent MeshVideoInfo

//var hover = false;
//var initialPos = new vec3.zero();
//var downPos = new vec3.zero();
//var time = 0;
var returned = false;

if (!script.heartsScene1)
	print("MySceneController: Please add reference to Mark to heartsScene1.");

if (!script.heartsScene2)
	print("MySceneController: Please add reference to Mark to heartsScene2.");

var audioComponent;
var animationPlayed = false;
var texturePlayed = false;

function audioSetup()
{
    if(script.audio && !audioComponent)
    {       
        audioComponent = script.getSceneObject().createComponent("Component.AudioComponent");
        audioComponent.audioTrack = script.audio;        
    }
}

function playAudio(audioComponent, loops)
{
    if (audioComponent)
    {
        if(audioComponent.isPlaying())
        {
            audioComponent.stop(false);
        }
        audioComponent.play( loops );
    }    
}

function stopAudio(audioComponent)
{
    if (audioComponent)
    {
        if(audioComponent.isPlaying())
        {
            audioComponent.stop(false);
        }
    }  
}

function loopHover1()
{
	if (script.heartsScene1)
		script.heartsScene1.startWithCallback("BaseLayer", script.MeshVideoInfo.api.videoDelay + 6.24, 1, loopHover1);
}
function loopHover2()
{
	if (script.heartsScene2)
		script.heartsScene2.startWithCallback("BaseLayer", script.MeshVideoInfo.api.videoDelay + 6.24, 1, loopHover2);
}

function playAnimationAndSound()
{
	if (script.heartsScene1)
		script.heartsScene1.startWithCallback("BaseLayer", 0, 1, loopHover1);
		
	if (script.heartsScene2)
		script.heartsScene2.startWithCallback("BaseLayer", 0, 1, loopHover2);
	
	playAudio(audioComponent, 1);
}

global.onSceneEnabled = function()
{	
	
	script.MeshVideoInfo.api.videoFinished ? returned = true : returned = false;
	
	animationPlayed = true;
	texturePlayed = false;
	
	script.getSceneObject().getChild(1).getTransform().setLocalPosition(new vec3(0,-100,0));
	script.getSceneObject().getChild(2).getTransform().setLocalPosition(new vec3(0,-100,0));

	playAnimationAndSound();
}

global.onSceneWillDisable = function()
{
	animationPlayed = false;
	texturePlayed = false;
	
	if (script.heartsScene1)
		script.heartsScene1.stop("");
	
	if (script.heartsScene2)
		script.heartsScene2.stop("");
	
	stopAudio(audioComponent);	
}

global.onSceneDisabled = function()
{
	if(script.MeshVideoInfo.api.allowHintEvent != null)
		script.MeshVideoInfo.api.allowHintEvent.reset(0);
}

function onUpdate() 
{
}
var updateEvent = script.createEvent("UpdateEvent");
updateEvent.bind(onUpdate)

audioSetup();