// MarkerWithSnapcodeController.js
// Version: 0.0.1
// Event: Lens Initialized
// Description: Plays Cross and Fade Animation, calls function when marker is lost or found. 

// @input bool enableSmoothing = true

//@ui {"widget":"separator"}
// @input bool advanced = false
// @input float smoothingAmount = 0.65 {"showIf":"advanced", "widget":"slider", "min":0.0, "max":1.0, "step":0.01}
// @input Component.ScriptComponent multipleTrackerController {"showIf":"advanced"}
// @input Component.ScriptComponent crossHintScript {"showIf": "advanced"}
// @input Component.ScriptComponent fadeEffectScript {"showIf": "advanced"}
// @input Component.ScriptComponent hintControllerScript {"showIf": "advanced"}

//@input SceneObject filtereffect

var isMarkerTracking;
var crossHintScript;
var disableNextFrame;
var smoothingSlerpFactor;
var markerHasBeenTracking;
var hintsComponent = script.getSceneObject().createComponent("Component.HintsComponent");

function onTurnOn() {
	
	global.touchSystem.touchBlocking = true;
	
	if (script.multipleTrackerController) 
	{
		script.multipleTrackerController.api.onMarkerFound = onMarkerFound;
		script.multipleTrackerController.api.onMarkerLost = onMarkerLost;
	}
	else
		print("MarkerWithSnapcodeController: Please assign multipleTrackerController.");

	if (script.fadeEffectScript && script.fadeEffectScript.api.resetFadeEffect)
		script.fadeEffectScript.api.resetFadeEffect();
	
	 if(global.scene.getCameraType() === "front")
	 {
		if (script.hintControllerScript && script.hintControllerScript.api.hide) 
			script.hintControllerScript.api.hide();
	 }
    else
    {
        if(script.hintControllerScript && script.hintControllerScript.api.show)
			script.hintControllerScript.api.show();
    }
	
	setChildrenEnabled(false);

	smoothingSlerpFactor = 1 - script.smoothingAmount;
	markerHasBeenTracking = false;
}

function onLateUpdate()
{
	if (isMarkerTracking)
	{
		if (script.multipleTrackerController && script.multipleTrackerController.api.getCurrentTracker)
		{
			var markerComponent = script.multipleTrackerController.api.getCurrentTracker();

			if (markerComponent)
			{
				var referenceObject = markerComponent.getSceneObject().getChild(0);
				copyTransformToSelf(referenceObject, markerHasBeenTracking);

				markerHasBeenTracking = true;
			}

		}
		else
			print("MarkerWithSnapcodeController: Please assign multipleTrackerController.");

	}
	
    if(script.hintControllerScript && script.hintControllerScript.api.show && global.scene.getCameraType() === "back" && !isMarkerTracking) 
		script.hintControllerScript.api.show();
	
    else if (script.hintControllerScript && script.hintControllerScript.api.hide && global.scene.getCameraType() === "front" )
		script.hintControllerScript.api.hide();
	
    if(global.scene.getCameraType() === "back")
    {
		global.scene.getRootObject(1).getChild(0).enabled = false;
        global.scene.getRootObject(2).getChild(1).getChild(0).enabled = false;
		global.scene.getRootObject(2).getChild(1).getChild(1).enabled = false;
        if(script.fadeEffectScript.api.allowHintAfterVideo)
		{
			hintsComponent.showHint("lens_hint_swap_camera", -1)
			global.touchSystem.touchBlocking = false;
		}
		else
		{
			hintsComponent.hideHint("lens_hint_swap_camera");
			global.touchSystem.touchBlocking = true;
		}
    }
	
	//if(global.scene.getCameraType() === "back" && script.fadeEffectScript.api.videoFinished)
	//	hintsComponent.showHint("lens_hint_swap_camera", -1);
	
    if(global.scene.getCameraType() === "front")
	{
		global.scene.getRootObject(1).getChild(0).enabled = true;
        global.scene.getRootObject(2).getChild(1).getChild(0).enabled = true;
		global.scene.getRootObject(2).getChild(1).getChild(1).enabled = true;

        if(!script.fadeEffectScript.api.videoFinished)
		{
			hintsComponent.showHint("lens_hint_swap_camera", -1);
			global.touchSystem.touchBlocking = false;
		}
        else if(script.fadeEffectScript.api.videoFinished && script.fadeEffectScript.api.allowHintAfterVideo)
		{
			//script.api.allowHintEvent.reset(0);
			script.fadeEffectScript.api.allowHintAfterVideo = false
			hintsComponent.hideHint("lens_hint_swap_camera");
			global.touchSystem.touchBlocking = true;
		}
		else if(script.fadeEffectScript.api.videoFinished && !script.fadeEffectScript.api.allowHintAfterVideo)
		{
			script.fadeEffectScript.api.allowHintEvent.reset(0);
		}
        
    }

	
	if (disableNextFrame) 
	{

		if (global.onSceneDisabled) {
			global.onSceneDisabled();
		}

		setChildrenEnabled(false);
		
		disableNextFrame = false;
		markerHasBeenTracking = false;
	}
}

function onMarkerFound() {
	isMarkerTracking = true;

	setChildrenEnabled(true);

	if (script.hintControllerScript && script.hintControllerScript.api.hide) {
		script.hintControllerScript.api.hide();
	}

	if (script.crossHintScript && script.crossHintScript.api.startCrossAnimation) {
		script.crossHintScript.api.startCrossAnimation();
	}

	if (script.fadeEffectScript && script.fadeEffectScript.api.startFade) {
		script.fadeEffectScript.api.startFade();
	}

	if (global.onSceneEnabled) {
		
		//if(hintsComponent)
		//	hintsComponent.hideHint("lens_hint_swap_camera");
		global.onSceneEnabled();
	}
}

function onMarkerLost() {
	isMarkerTracking = false;

	if (script.hintControllerScript && script.hintControllerScript.api.show && !script.fadeEffectScript.api.videoFinished) {
		script.hintControllerScript.api.show();
	}
   
	if (global.onSceneWillDisable) {
		global.onSceneWillDisable();
	}
	
	disableNextFrame = true;
}

function copyTransformToSelf(o, shouldSmooth) {
	var sourceT = o.getTransform();
	var targetT = script.getTransform();

	var nextPos = sourceT.getWorldPosition();
	var nextRot = sourceT.getWorldRotation();
	var nextScale = sourceT.getWorldScale();

	if (script.enableSmoothing && shouldSmooth) {
		var currentRot = targetT.getWorldRotation();
		nextRot = quat.slerp(currentRot, nextRot, smoothingSlerpFactor);
	}

	targetT.setWorldPosition(nextPos);
	targetT.setWorldRotation(nextRot);
	targetT.setWorldScale(nextScale);
}

function setChildrenEnabled (enabled) {
	for (var i = 0; i < script.getSceneObject().getChildrenCount(); i++) {
		script.getSceneObject().getChild(i).enabled = enabled;
	}
}

onTurnOn();

var lateUpdateEvent = script.createEvent("LateUpdateEvent");
lateUpdateEvent.bind(onLateUpdate);