// -----JS CODE-----
var debug = false;

//@input SceneObject faceStretch
//@input SceneObject sprite
//@input SceneObject mouthTracker
//@input float maxTime = 1.0 {"label":"TimeToMouth", "widget":"slider", "min":0.0, "max":10.0, "step":0.01}
//@input float deltaDistance = 0.2 {"label":"FoodInMouthDelta", "widget":"slider", "min":0.0, "max":10.0, "step":0.01}
//@input Asset.Texture[] spriteTex

var foodPosition = script.sprite.getTransform().getWorldPosition();
var hintsComponent = script.getSceneObject().createComponent("Component.HintsComponent");

var obj = script.faceStretch.getChild(0);
var comp = obj.getFirstComponent("Component.FaceStretchVisual")

var foodMoving = false;
var time = 0.0;

initialize();

function initialize()
{
	comp.setFeatureWeight("Feature0", 0.0);
	comp.setFeatureWeight("Feature1", 0.0);
	comp.setFeatureWeight("Feature2", 0.0);
}

function increaseFaceSize()
{	
	var feature = "Feature0"; //0.5
	var intensity = comp.getFeatureWeight(feature);	
	intensity > 1 ? comp.setFeatureWeight(feature, 1) : comp.setFeatureWeight(feature, intensity + 0.1);
	
	feature = "Feature1"; // 2
	intensity = comp.getFeatureWeight(feature);	
	intensity > 1 ? comp.setFeatureWeight(feature, 1) : comp.setFeatureWeight(feature, intensity + 0.1);
	
	feature = "Feature2"; //0.5
	intensity = comp.getFeatureWeight(feature);	
	intensity > 1 ? comp.setFeatureWeight(feature, 1) : comp.setFeatureWeight(feature, intensity + 0.1);
}
script.api.IncreaseFaceSize = increaseFaceSize;

// Play animation and morph face con mouth open
function MouthOpen()
{
	increaseFaceSize();
	foodMoving = true;
}
var mouthopenevent = script.createEvent("MouthOpenedEvent");
mouthopenevent.bind(MouthOpen);

// Request camera swap
function CameraBack()
{
	hintsComponent.showHint("lens_hint_swap_camera", -1);
}
var cameraBack = script.createEvent("CameraBackEvent");
cameraBack.bind(CameraBack);

//Instruct to open mouth
function CameraFront()
{
	hintsComponent.showHint("lens_hint_open_your_mouth", 1);
}
var cameraFront = script.createEvent("CameraFrontEvent");
cameraFront.bind(CameraFront);

// Linearly interpolate the position of the food and select a new meal after each full interpolation
function UpdateEvent()
{
	if(foodMoving)
	{
		if(time >= script.maxTime || script.sprite.getTransform().getWorldPosition().distance(script.mouthTracker.getTransform().getWorldPosition()) < script.FoodInMouthDelta)
		{
			time = 0.0;
			foodMoving = false;
			script.sprite.getTransform().setWorldPosition(foodPosition);
			
			script.sprite.getFirstComponent("Component.SpriteVisual").mainPass.baseTex = script.spriteTex[getRandomInRange(0,script.spriteTex.length)];
		}
		
		var normalized_t = time / script.maxTime;
		var interpolated_position = vec3.lerp(foodPosition, script.mouthTracker.getTransform().getWorldPosition(), normalized_t);		
		
		script.sprite.getTransform().setWorldPosition(interpolated_position);
		time += getDeltaTime();
	}
}
var updateEvent = script.createEvent("UpdateEvent")
updateEvent.bind(UpdateEvent);

/**
 * Returns a random number between min (inclusive) and max (exclusive)
 */
function getRandomInRange(min, max)
{
   min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min;
}
