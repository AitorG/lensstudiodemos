// -----JS CODE-----

//@input Asset.ObjectPrefab candyPrefab
//@input Component.ScriptComponent PackageState1
//@input Component.ScriptComponent PackageState2
//@input Component.Camera Orthocamera
//@input Component.Camera camera
var candyArray = [];
var velocityArray = [];
var count = 0;

var usedBox = -1; // -1 null state, 0 right candy box, 1 left candy box

//----------------------
// EVENTS
//----------------------

function onCandiesOutEvent(eventData)
{
	print("All out above");
	for(var i= 0; i < candyArray.length; i++)
	{
		var x = Math.random();
		var y = Math.random()*3;		
		var z = Math.random()*10;
		
		var pos = new vec2(x,y);
		//print(i+": "+pos+" "+z);
		
		var nwp = script.camera.screenSpaceToWorldSpace(pos, script.camera.getTransform().getWorldPosition().z+z);
		//screen space y=1 is bottom, world space y+>>0 is top
		nwp.y = -nwp.y;
		
		//set new candy position
		//print(i+": "+nwp);
		candyArray[i].getTransform().setWorldPosition(nwp);
		velocityArray[i] = new vec3(0,getRandomFloat(-7,-3),0);
	}
	candiesOutEvent.enabled = false;
}
var candiesOutEvent = script.createEvent("DelayedCallbackEvent")
candiesOutEvent.bind(onCandiesOutEvent);

function onCandiesFallenEvent(eventData)
{
	print("All out below");
	for(var i= 0; i < candyArray.length; i++)
		candyArray[i].destroy();
	
	candyArray.length = 0;
	velocityArray.length = 0;
	count = 0;
	candiesOutEvent.enabled = true;	
	
	
	
	//script.PackageState1.api.toggleTapsEvent.data = {enabled : true};
	//script.PackageState1.api.toggleTapsEvent.reset(0);
	//script.PackageState2.api.toggleTapsEvent.data = {enabled : true};
	//script.PackageState2.api.toggleTapsEvent.reset(0);
	
}
var candiesFallenEvent = script.createEvent("DelayedCallbackEvent")
candiesFallenEvent.bind(onCandiesFallenEvent);

function onFrameUpdated( )
{
	if(script.PackageState1.api.isTapped)
	{
		// grab the position of the package in woorld coordinates
		var wp = script.PackageState1.getSceneObject().getChild(0).getTransform().getWorldPosition();
		// convert to normalized screen coordinates relative to the orthographic camera rendering the HUD
		var sp = script.Orthocamera.worldSpaceToScreenSpace(wp);
		// revert back to world coordinates through the regular camera using the screen coordinates and the distance from the camera to the HUD
		var nwp = script.camera.screenSpaceToWorldSpace(sp, script.camera.getTransform().getWorldPosition().z);

		script.PackageState1.api.toggleTapsEvent.data = {enabled : false};
		script.PackageState1.api.toggleTapsEvent.reset(0);
		//script.PackageState2.api.toggleTapsEvent.data = {enabled : false};
		//script.PackageState2.api.toggleTapsEvent.reset(0);
		script.PackageState2.getSceneObject().enabled = false;

		instanciateCandy(nwp, true);
	}
	if(script.PackageState2.api.isTapped)
	{
		// grab the position of the package in woorld coordinates
		var wp = script.PackageState2.getSceneObject().getChild(0).getTransform().getWorldPosition();
		// convert to normalized screen coordinates relative to the orthographic camera rendering the HUD
		var sp = script.Orthocamera.worldSpaceToScreenSpace(wp);
		// revert back to world coordinates through the regular camera using the screen coordinates and the distance from the camera to the HUD
		var nwp = script.camera.screenSpaceToWorldSpace(sp, script.camera.getTransform().getWorldPosition().z);
		
		script.PackageState1.api.toggleTapsEvent.data = {enabled : false};
		script.PackageState1.api.toggleTapsEvent.reset(0);
		script.PackageState2.api.toggleTapsEvent.data = {enabled : false};
		script.PackageState2.api.toggleTapsEvent.reset(0);
		
		instanciateCandy(nwp, false);
	}
	
	// use this function to check if ALL the candies have gone out of the screen space either from the top or the bottom part
	(candiesOutEvent.enabled == true) ? checkAllCandyPos() : checkAllCandyDownPos();
		
	for(var i= 0; i < candyArray.length; i++)
	{
		var pos = candyArray[i].getTransform().getWorldPosition();
		
		var newPos = pos.add(velocityArray[i]);
		candyArray[i].getTransform().setWorldPosition(newPos)
	}
}
var updateEvent = script.createEvent("UpdateEvent");
updateEvent.bind(onFrameUpdated);

//----------------------
// HELPER METHODS
//----------------------

function checkAllCandyPos()
{
	if(candyArray.length > 0 )
	{
		for(var i= 0; i < candyArray.length; i++)
		{
			var pos = script.camera.worldSpaceToScreenSpace(candyArray[i].getTransform().getWorldPosition()).y;		
			if( pos > 0)
				return;
		}		
		candiesOutEvent.reset(0);
	}
}

function checkAllCandyDownPos()
{
	if(candyArray.length > 0 )
	{
		for(var i= 0; i < candyArray.length; i++)
		{
			var pos = script.camera.worldSpaceToScreenSpace(candyArray[i].getTransform().getWorldPosition()).y;		
			if( pos < 1)
				return;
		}
		candiesFallenEvent.reset(0);
	}
}

function instanciateCandy( worldPos,right )
{
	// safeguard for default params
	worldPos = (typeof worldPos !== 'undefined') ?  worldPos : new vec3(0,0,0);
	
	var object = script.candyPrefab.instantiate(null);
	object.getTransform().setWorldPosition(worldPos);
	
	newRot = createRandomRotation();
	object.getTransform().setWorldRotation(newRot);
	
	object.name = "candy"+(count);
	candyArray.push(object);
	count++;
	
	var eulerAnglesInRad;
	var dir;
	if(right)
	{
		eulerAnglesInRad= script.PackageState1.getSceneObject().getTransform().getWorldRotation().toEulerAngles();
		dir = new vec3(-1+Math.cos(eulerAnglesInRad.z), Math.sin(eulerAnglesInRad.z),0);
	}
	else
	{
		eulerAnglesInRad= script.PackageState2.getSceneObject().getTransform().getWorldRotation().toEulerAngles();
		dir = new vec3(1-Math.cos(eulerAnglesInRad.z), -Math.sin(eulerAnglesInRad.z),0);
	}
	
	velocityArray.push(dir.uniformScale(12));
}

function createRandomRotation( )
{
	var x = Math.random();
	var y = Math.random();
	var z = Math.random();
	var w = Math.random();
	
	var q = new quat(w,x,y,z);	
	q.normalize();
	
	return q;
}

function getRandomFloat(min, max)
{  
  return (Math.random() * (max - min)) + min; //The maximum is exclusive and the minimum is inclusive
}
