// -----JS CODE-----
// @input SceneObject attachToObject
// @input bool attachX
// @input bool attachY
// @input bool attachZ
// @input vec3 offset

var attachToObjectPosition = script.attachToObject.getTransform().getWorldPosition();
var newPosX = 0.0;
var newPosY = 0.0;
var newPosZ = 0.0;

if( script.attachX ) {
    newPosX = attachToObjectPosition.x + script.offset.x;
}

if( script.attachY ) {
    newPosY = attachToObjectPosition.y + script.offset.y;
}

if( script.attachZ ) {
    newPosZ = attachToObjectPosition.z + script.offset.z;
}

script.getTransform().setWorldPosition( new vec3( newPosX, newPosY, newPosZ ) );