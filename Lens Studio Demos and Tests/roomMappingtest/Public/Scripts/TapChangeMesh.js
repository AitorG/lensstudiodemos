// -----JS CODE-----

//@input SceneObject mesh
//@input Component.Label TextChanger

var Sizes = {"onebyone":1, "twobyone":2, "threebyone":3, "threebytwo":4, "size":5};
id = Sizes.onebyone;

// External event
function onTapEvent(eventData)
{
	id++;
	if(id == Sizes.size)
		id = 0;
	switch(id)
	{
		case Sizes.onebyone:
			script.mesh.getTransform().setLocalScale(new vec3(10, 10, 10));
			script.TextChanger.text = "1x1";
			break;
		case Sizes.twobyone:
			script.mesh.getTransform().setLocalScale(new vec3(10, 20, 10));
			script.TextChanger.text = "2x1";
			break;
		case Sizes.threebyone:
			script.mesh.getTransform().setLocalScale(new vec3(10, 30, 10));
			script.TextChanger.text = "3x1";
			break;
		case Sizes.threebytwo:
			script.mesh.getTransform().setLocalScale(new vec3(20, 30, 10));
			script.TextChanger.text = "3x2";
			break;
		default:
			script.mesh.getTransform().setLocalScale(new vec3(10, 10, 10));
			script.TextChanger.text = "1x1";
			break;
	}
}
var tapEvent = script.createEvent("TapEvent")
tapEvent.bind(onTapEvent);