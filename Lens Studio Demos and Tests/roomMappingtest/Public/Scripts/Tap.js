// -----JS CODE-----
// @input SceneObject[] MyGroupTextArray
// @input SceneObject[] OtherGroupTextArray
// @input SceneObject textureHolder

// @input SceneObject SizeChanger

for(var i = 0; i < script.MyGroupTextArray.length; i++)
	script.MyGroupTextArray[i].enabled = true;

for(var i = 0; i < script.OtherGroupTextArray.length; i++)
	script.OtherGroupTextArray[i].enabled = false;

// External event
function onTextureSelectedEvent(eventData)
{
	var texture = script.api.textureSelectedEvent.data.texture;
	
	script.textureHolder.getFirstComponent("Component.MeshVisual").mainPass.baseTex = texture;	
	script.SizeChanger.getFirstComponent("Component.SpriteVisual").mainPass.baseTex = texture;
	
	for(var i = 0; i < script.MyGroupTextArray.length; i++)
		script.MyGroupTextArray[i].enabled = false;

	for(var i = 0; i < script.OtherGroupTextArray.length; i++)
		script.OtherGroupTextArray[i].enabled = false;
}
script.api.textureSelectedEvent = script.createEvent("DelayedCallbackEvent")
script.api.textureSelectedEvent.bind(onTextureSelectedEvent);