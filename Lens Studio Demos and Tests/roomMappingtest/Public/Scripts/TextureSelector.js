// -----JS CODE-----

//@input Component.ScriptComponent Tap

var _texture = script.getSceneObject().getFirstComponent("Component.SpriteVisual").mainPass.baseTex;
script.Tap.api.textureSelectedEvent.reset(0);

// Technological breakthrough passing data on the custom events
script.Tap.api.textureSelectedEvent.data = {texture : _texture};
