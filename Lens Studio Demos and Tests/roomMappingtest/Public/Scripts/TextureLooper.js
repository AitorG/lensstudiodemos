// -----JS CODE-----
//@input SceneObject TouchCollison

var pass = script.getSceneObject().getFirstComponent("Component.MeshVisual").mainPass;

var scalex = script.getSceneObject().getParent().getParent().getTransform().getLocalScale().x * 0.1 * script.getSceneObject().getTransform().getLocalScale().x;
var scaley = script.getSceneObject().getParent().getParent().getTransform().getLocalScale().z * 0.1 * script.getSceneObject().getTransform().getLocalScale().y;

// 0.25 = 2.5 (initial defined scale, fixed) by 0.1 to counteract the scale 10 of the "unit" plane
script.TouchCollison.getTransform().setLocalScale(new vec3(0.25*script.getSceneObject().getTransform().getLocalScale().x, 0.5, 0.25*script.getSceneObject().getTransform().getLocalScale().z));

//global.logToScreen(scalex+" "+scaley);

pass.uv2Scale = new vec2(scalex, scaley);

