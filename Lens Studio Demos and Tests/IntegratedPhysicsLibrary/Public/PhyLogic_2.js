// -----JS CODE-----
//@input Component.ScriptComponent CannonWorld

(
function simloop()
{
  
	if(script.CannonWorld.api.lastTime !== undefined)
	{	
		print("DT: "+script.CannonWorld.api.dt);
		//var dt = (getTime() - script.CannonWorld.api.lastTime) / 1000;
		var dt = script.CannonWorld.api.fixedTimeStep;
		script.CannonWorld.api.dt = dt;
		//dt =  script.CannonWorld.api.dt;
		script.CannonWorld.api.world.step(script.CannonWorld.api.fixedTimeStep, dt, script.CannonWorld.api.maxSubSteps);
		
		script.CannonWorld.api.boxBody.position.x = script.CannonWorld.api.boxBody.position.x + script.CannonWorld.api.boxBody.velocity.x*dt+0.5*dt*dt*script.CannonWorld.api.gravity.x;
		script.CannonWorld.api.boxBody.position.y = script.CannonWorld.api.boxBody.position.y + script.CannonWorld.api.boxBody.velocity.y*dt+0.5*dt*dt*script.CannonWorld.api.gravity.y;
		script.CannonWorld.api.boxBody.position.z = script.CannonWorld.api.boxBody.position.z + script.CannonWorld.api.boxBody.velocity.z*dt+0.5*dt*dt*script.CannonWorld.api.gravity.z;
		
		script.getSceneObject().getTransform().setLocalPosition(new vec3(script.CannonWorld.api.boxBody.position.x, script.CannonWorld.api.boxBody.position.y, script.CannonWorld.api.boxBody.position.z));
		//script.getSceneObject().getTransform().setLocalPosition(new vec3(100, -50, -6 ));
	}
	print("Box x velocity: " + script.CannonWorld.api.boxBody.velocity.x);
	print("Box y velocity: " + script.CannonWorld.api.boxBody.velocity.y);
	print("Box z velocity: " + script.CannonWorld.api.boxBody.velocity.z);
	
	print("Box x position: " + script.CannonWorld.api.boxBody.position.x);
	print("Box y position: " + script.CannonWorld.api.boxBody.position.y);
	print("Box z position: " + script.CannonWorld.api.boxBody.position.z);
	script.CannonWorld.api.lastTime = getTime();
})();


function OnTap()
{
	//var f = 5000;
	//var dt = script.CannonWorld.api.dt;
	//var worldPoint = new CANNON.Vec3(script.CannonWorld.api.boxBody.position.x, script.CannonWorld.api.boxBody.position.y,script.CannonWorld.api.boxBody.position.z);
	//var impulse = new CANNON.Vec3(f*dt,0,0);
	//print(impulse.x+" "+impulse.y+" "+impulse.z);
	//script.CannonWorld.api.boxBody.applyImpulse(impulse,worldPoint);
	print("TAP");
	script.CannonWorld.api.boxBody.velocity = new CANNON.Vec3(20,0,0);
}
var OnTapEvent = script.createEvent("TapEvent");
OnTapEvent.bind(OnTap);