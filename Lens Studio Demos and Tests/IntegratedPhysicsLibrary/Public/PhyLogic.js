// -----JS CODE-----
//@input Component.ScriptComponent CannonWorld

(
function simloop()
{
  
	if(script.CannonWorld.api.lastTime !== undefined)
	{	
		print("DT: "+script.CannonWorld.api.dt);
		//var dt = (getTime() - script.CannonWorld.api.lastTime) / 1000;
		var dt = script.CannonWorld.api.fixedTimeStep;
		script.CannonWorld.api.dt = dt;
		//dt =  script.CannonWorld.api.dt;
		script.CannonWorld.api.world.step(script.CannonWorld.api.fixedTimeStep, dt, script.CannonWorld.api.maxSubSteps);
		
		script.CannonWorld.api.sphereBody.position.x = script.CannonWorld.api.sphereBody.position.x + script.CannonWorld.api.sphereBody.velocity.x*dt+0.5*dt*dt*script.CannonWorld.api.gravity.x;
		script.CannonWorld.api.sphereBody.position.y = script.CannonWorld.api.sphereBody.position.y + script.CannonWorld.api.sphereBody.velocity.y*dt+0.5*dt*dt*script.CannonWorld.api.gravity.y;
		script.CannonWorld.api.sphereBody.position.z = script.CannonWorld.api.sphereBody.position.z + script.CannonWorld.api.sphereBody.velocity.z*dt+0.5*dt*dt*script.CannonWorld.api.gravity.z;
		
		script.getSceneObject().getTransform().setLocalPosition(new vec3(script.CannonWorld.api.sphereBody.position.x, script.CannonWorld.api.sphereBody.position.y, script.CannonWorld.api.sphereBody.position.z ));
	}
	print("Sphere x velocity: " + script.CannonWorld.api.sphereBody.velocity.x);
	print("Sphere y velocity: " + script.CannonWorld.api.sphereBody.velocity.y);
	print("Sphere z velocity: " + script.CannonWorld.api.sphereBody.velocity.z);
	
	print("Sphere x position: " + script.CannonWorld.api.sphereBody.position.x);
	print("Sphere y position: " + script.CannonWorld.api.sphereBody.position.y);
	print("Sphere z position: " + script.CannonWorld.api.sphereBody.position.z);
	script.CannonWorld.api.lastTime = getTime();
})();


function OnTap()
{
	//var f = 5000;
	//var dt = script.CannonWorld.api.dt;
	//var worldPoint = new CANNON.Vec3(script.CannonWorld.api.sphereBody.position.x, script.CannonWorld.api.sphereBody.position.y,script.CannonWorld.api.sphereBody.position.z);
	//var impulse = new CANNON.Vec3(f*dt,0,0);
	//print(impulse.x+" "+impulse.y+" "+impulse.z);
	//script.CannonWorld.api.sphereBody.applyImpulse(impulse,worldPoint);
	print("TAP");
	script.CannonWorld.api.sphereBody.velocity = new CANNON.Vec3(-20,0,0);
}
var OnTapEvent = script.createEvent("TapEvent");
OnTapEvent.bind(OnTap);