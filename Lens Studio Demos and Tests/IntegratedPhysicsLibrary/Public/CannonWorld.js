// -----JS CODE-----
// Setup our world
var world = new CANNON.World();
world.gravity.set(0, 0, -9.82); // m/s²
script.api.gravity = world.gravity;
script.api.world = world;
script.api.dt = world.dt;


// Create a sphere
var radius = 1; // m
var sphereBody = new CANNON.Body({
   mass: 5, // kg
   position: new CANNON.Vec3(0, 0, 40), // m
   shape: new CANNON.Sphere(radius)
});
world.addBody(sphereBody);
script.api.sphereBody = sphereBody;

var boxBody = new CANNON.Body({
	mass: 0,
	position: new CANNON.Vec3(0,0,15),
	shape: new CANNON.Box(new CANNON.Vec3(2,1,1))
});
world.addBody(boxBody);
script.api.boxBody = boxBody;

// Create a plane
var groundBody = new CANNON.Body({
    mass: 0 // mass == 0 makes the body static
});

var groundShape = new CANNON.Plane();
groundBody.addShape(groundShape);
world.addBody(groundBody);

script.api.fixedTimeStep = 1.0 / 60.0; // seconds
script.api.maxSubSteps = 3;

// Start the simulation loop
script.api.lastTime;
