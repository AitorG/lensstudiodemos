/********************************************************************/
/*!
\file   ExtruderVertical.js
\author Aitor Gandia
\par    Visual Engineering AS
\date   Mar 2019

\brief
Wall Generator, uses the ground defined markers to build walls between 2 aligned nodes

Copyright (C) 2019 Visual Engineering AS.
Reproduction or disclosure of this file or its contents
without the prior written consent of Visual Engineering AS is prohibited.
*/
/*********************************************************************/

//@input Component.ScriptComponent CubeGenerator
//@input Component.Camera camera

//@ui {"widget":"separator"}
//@ui {"widget":"label", "label":"Extruder Label"}
// Empty label, adds empty space
//@input SceneObject arrowup
//@input SceneObject arrowdown
//@ui {"widget":"label"}

var extrude = false;
var touchedCubeName = "";

function TouchStarted(eventData)
{
	var comp = script.getSceneObject().getFirstComponent("Component.ManipulateComponent");
	if(comp.enabled == false)
	{
		var _worldPos = script.getSceneObject().getTransform().getWorldPosition();
		
		script.CubeGenerator.api.print(script.getSceneObject().name);
		touchedCubeName = script.getSceneObject().name;
		
		var _sspFromCamera = script.camera.worldSpaceToScreenSpace(_worldPos);
		
		script.arrowup.enabled = true;
		script.arrowdown.enabled = true;
	}
}
var onTouchStartedEvent = script.createEvent("TouchStartEvent");
onTouchStartedEvent.bind(TouchStarted);

function TouchEnded(eventData)
{
	var comp = script.getSceneObject().getFirstComponent("Component.ManipulateComponent");
	if(comp.enabled == false)
	{
		script.arrowup.enabled = false;
		script.arrowdown.enabled = false;
		
		touchedCubeName = "";
		
		
	}
}
var onTouchEndedEvent = script.createEvent("TouchEndEvent");
onTouchEndedEvent.bind(TouchEnded);

function UpdateEvent(eventData)
{
	//Update Arrow Label Positions
	if(script.arrowup.enabled == true && script.arrowdown.enabled == true && script.getSceneObject().name == touchedCubeName)
	{
		var _worldPos = script.getSceneObject().getTransform().getWorldPosition();
		var _sspFromCamera = script.camera.worldSpaceToScreenSpace(_worldPos);
				
		// make one arrow a bit above the cube
		var offsetpos =_sspFromCamera.scale(new vec2(2,-2)).sub(new vec2(1,-1.1));
		script.arrowup.getFirstComponent("Component.SpriteAligner").bindingPoint = offsetpos;
		// the other arrow a bit under the cube		
		offsetpos = _sspFromCamera.scale(new vec2(2,-2)).sub(new vec2(1,-0.9));
		script.arrowdown.getFirstComponent("Component.SpriteAligner").bindingPoint = offsetpos;
	}
}
var onUpdateEvent = script.createEvent("UpdateEvent")
onUpdateEvent.bind(UpdateEvent);