/********************************************************************/
/*!
\file   RemoveLastObject.js
\author Aitor Gandia
\par    Visual Engineering AS
\date   Mar 2019

\brief
Handles the destruction of Cubes on the Marker array
if the removed cube belonged to a generated plane, we also remove the plane

Copyright (C) 2019 Visual Engineering AS.
Reproduction or disclosure of this file or its contents
without the prior written consent of Visual Engineering AS is prohibited.
*/
/*********************************************************************/

//@input Component.ScriptComponent CubeGenerator

if(script.CubeGenerator.api.CubeArray.length>0)
{
	//fetch the last cube on the array
	var obj = script.CubeGenerator.api.CubeArray[script.CubeGenerator.api.CubeArray.length-1];
	
	script.CubeGenerator.api.CubeArray.pop();
	script.CubeGenerator.api.count--;
	
	//Remove Child objects, if there is children within children to any depth this will crash and needs to be done recursivelly.	
	for(var j = obj.getChildrenCount()-1; j >= 0; j--)
	{
		var child = obj.getChild(j);
		for(var i = child.getComponentCount("")-1; i >= 0; i--)
			child.getComponentByIndex("",i).destroy();
		
		child.destroy();
	}
	// Then clean the object's components
	for(var i = obj.getComponentCount("")-1; i > 0; i--)
			obj.getComponentByIndex("",i).destroy();
	
	// Finally remove the object from the scene
	var objsce = global.scene.getRootObject(global.scene.getRootObjectsCount()-1);
	obj.destroy();
	
	// If we remove the corner of a cube that was generating a plane, remove said plane
	if(script.CubeGenerator.api.PlaneArray.length > 0 &&
		script.CubeGenerator.api.CubeArray.length < script.CubeGenerator.api.PlaneArray.length * 4 )
	{
		//fetch the last cube on the array
		var obj = script.CubeGenerator.api.PlaneArray[script.CubeGenerator.api.PlaneArray.length-1];
		script.CubeGenerator.api.PlaneArray.pop();
	
		// Then clean the object's components
		for(var j = obj.getComponentCount("")-1; j > 0; i--)
			obj.getComponentByIndex("",j).destroy();
	
		// Finally remove the object from the scene
		var objsce = global.scene.getRootObject(global.scene.getRootObjectsCount()-1);
		
		obj.destroy();
	}
}
else
	script.CubeGenerator.api.print("You cant remove the last object");