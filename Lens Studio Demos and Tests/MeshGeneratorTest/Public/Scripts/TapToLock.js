/********************************************************************/
/*!
\file   TapToLock.js
\author Aitor Gandia
\par    Visual Engineering AS
\date   Mar 2019

\brief
Disable ManipulateComponent out of the tapped cubes

Copyright (C) 2019 Visual Engineering AS.
Reproduction or disclosure of this file or its contents
without the prior written consent of Visual Engineering AS is prohibited.
*/
/*********************************************************************/
//@input Component.ScriptComponent CubeGenerator

var comp = script.getSceneObject().getFirstComponent("Component.ManipulateComponent");
if(comp && comp.enabled == true)
{
	comp.enabled = false;
	var _worldpos = script.getSceneObject().getTransform().getWorldPosition();

	//var _string = "Marker Position: " + _worldpos.x +" "+ _worldpos.y+" "+_worldpos.z;	
	//script.CubeGenerator.api.print(_string);
	
	script.CubeGenerator.api.instanciateCube(new vec3(0,0,0));
}
