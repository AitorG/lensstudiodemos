/********************************************************************/
/*!
\file   MeshGenerator.js
\author Aitor Gandia
\par    Visual Engineering AS
\date   Mar 2019

\brief
Removes the last wall that was created

Copyright (C) 2019 Visual Engineering AS.
Reproduction or disclosure of this file or its contents
without the prior written consent of Visual Engineering AS is prohibited.
*/
/*********************************************************************/

//@input Component.ScriptComponent CubeGenerator

script.CubeGenerator.api.removeWallsEvent.reset(0);
