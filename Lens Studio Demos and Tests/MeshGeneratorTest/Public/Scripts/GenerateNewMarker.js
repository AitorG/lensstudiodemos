/********************************************************************/
/*!
\file   GenerateNewMarker.js
\author Aitor Gandia
\par    Visual Engineering AS
\date   Mar 2019

\brief
Keeps an array to all the static generated markers and a pointer to the current movable marker
Handle events that relate to static marker manipulation
Keeps and handles events related to create ground planes when cubes are added or removed;

Copyright (C) 2019 Visual Engineering AS.
Reproduction or disclosure of this file or its contents
without the prior written consent of Visual Engineering AS is prohibited.
*/
/*********************************************************************/

//@input Asset.ObjectPrefab markerPrefab
//@input Asset.ObjectPrefab planePrefab

// External public variables
script.api.count = 0;
script.api.clickPosition = new vec2(0,0);
script.api.CubeArray = [];
script.api.PlaneArray = [];
script.api.planePrefab = script.planePrefab;

// External public methods
script.api.print = printf;
script.api.instanciateCube = InstanciateCube;
// private variables
var movingCube = global.scene.getRootObject(4);

// Printable
function printf(msg)
{
	global.logToScreen(msg);
}

// Push moving cube to array of cubes, generate new moving cube if the new length of cubearray is mod4 we autogenerate a new ground plane.
function InstanciateCube(worldPos)
{
	var object = script.markerPrefab.instantiate(null);		
	object.getTransform().setWorldPosition(worldPos);
	
	//clone the material, so each cube can have an individual base color;
	object.getChild(0).getFirstComponent("Component.MeshVisual").mainMaterial = object.getChild(0).getFirstComponent("Component.MeshVisual").mainMaterial.clone();
	// change material base color
	object.getChild(0).getFirstComponent("Component.MeshVisual").mainPass.baseColor = new vec4(Math.random(), Math.random(), Math.random(), 1);
	
	if(movingCube)
	{
		script.api.CubeArray.push(movingCube);
		script.api.count++;
		//printf("Cubes in array: " + this.count);
	}
	else
		printf("ERROR: Could not find movingCube in GenerateNewMarker fn(InstantiateCube)");
	
	object.name = "WorldMarker"+(this.count);
	movingCube = object;
	
	// here we could check for CubeArray.length % 4 == 0 and autogenerate the floor plane, removing one button from the ui
	if(script.api.CubeArray.length % 4 == 0)
		GeneratePlanesEvent();
}

//Given four cubes generates a plane centered respect to those cubes, rotated to be aligned and scaled
function GeneratePlanesEvent(eventData)
{
	
// REMOVE ALL PLANES AND RE-GENERATE
var number_of_planes = script.api.PlaneArray.length-1;

for(var i = number_of_planes; i >= 0; i--)
{
	//fetch the last cube on the array
	var obj = script.api.PlaneArray[i];
	script.api.PlaneArray.pop();
	
	// Then clean the object's components
	for(var j = obj.getComponentCount("")-1; j > 0; i--)
			obj.getComponentByIndex("",j).destroy();
	
	// Finally remove the object from the scene
	var objsce = global.scene.getRootObject(global.scene.getRootObjectsCount()-1);
	
	obj.destroy();
}

if(script.api.count >= 4)
{	
	for(var i = 3; i< script.api.count; i+=4)
	{
		// from the corners 
		var corner0 = script.api.CubeArray[i-3].getTransform().getWorldPosition();
		var corner1 = script.api.CubeArray[i-2].getTransform().getWorldPosition();
		var corner2 = script.api.CubeArray[i-1].getTransform().getWorldPosition();
		var corner3 = script.api.CubeArray[i].getTransform().getWorldPosition();
		
		// Translate
		if(true)
		{
			// find inscribed cuadrilateral
			var midpointEdge0 = corner0.add(corner1.sub(corner0).uniformScale(0.5));
			var midpointEdge1 = corner1.add(corner2.sub(corner1).uniformScale(0.5));
			var midpointEdge2 = corner2.add(corner3.sub(corner2).uniformScale(0.5));
			var midpointEdge3 = corner3.add(corner0.sub(corner3).uniformScale(0.5));
			
			//find the center of the inscribed quadrilateral
			var center = midpointEdge0.add(midpointEdge1);
			center = center.add(midpointEdge2);
			center = center.add(midpointEdge3);
			center= center.uniformScale(0.25);
			
			// create a plane in the midpoint of all 4 markers
			var plane = script.api.planePrefab.instantiate(null);
			plane.getTransform().setWorldPosition(new vec3(center.x, 0.01, center.z));
		}
		//rotate
		if(true)
		{
			var leadingVec = corner1.sub(corner0).normalize();
			
			var axis = vec3.right();
			var rad = leadingVec.angleTo(axis);
			
			if(leadingVec.z > 0)
				rad = 2*Math.PI - rad;

			var old = plane.getTransform().getWorldRotation().toEulerAngles();
			plane.getTransform().setWorldRotation(quat.fromEulerAngles(old.x,rad,old.z));
		}
		// Scale
		if(true)
		{
			var lateral_dist = Math.max(Math.abs(corner0.x - corner1.x), Math.abs(corner0.z - corner1.z));
			var frontal_dist = Math.max(Math.min(Math.abs(corner1.z - corner2.z), Math.abs(corner1.z - corner3.z)), Math.min(Math.abs(corner1.x - corner2.x), Math.abs(corner1.x - corner3.x)));
			
			//7.5 is the displacement from the center to a vertex in real world units of a 1x1x1 cube in lens studio
			// meaning in this context that the scale of a plane is distance(left corner to right corner)/ 2x7.5
			var scalex = ((lateral_dist+7.5)/15);// remove +7.5 if the CORNER of the marker cube is to delimit the plane, otherwise the CENTER of the cube is the reference point
			var scalez = ((frontal_dist+7.5)/15);// remove +7.5 if the CORNER of the marker cube is to delimit the plane, otherwise the CENTER of the cube is the reference point
			
			plane.getTransform().setWorldScale(new vec3(scalex, scalez, 1));
		}
		
		script.api.PlaneArray.push(plane);
		plane.name = "Plane" + Math.floor(i/4);
	}
}
else
	script.api.print("Not enough corner markers, unable to generate mesh");
}
script.api.generatePlanesEvent = script.createEvent("DelayedCallbackEvent")
script.api.generatePlanesEvent.bind(GeneratePlanesEvent);

// destroys a wall plane
function removeLastWall(eventData)
{
	
}
script.api.removeWallsEvent = script.createEvent("DelayedCallbackEvent")
script.api.removeWallsEvent.bind(removeLastWall);


/*****************************************************************************/
//EVENTS
/*****************************************************************************/

// Touch Start Event
function TouchStarted(eventData)
{
	
}
var onTouchStartedEvent = script.createEvent("TouchStartEvent");
onTouchStartedEvent.bind(TouchStarted);

// Touch Start Event
function TouchMove(eventData)
{
	
}
var onTouchMoveEvent = script.createEvent("TouchStartEvent");
onTouchMoveEvent.bind(TouchMove);


// Touch End Event
function TouchEnded(eventData)
{
	
}
var onTouchEndedEvent = script.createEvent("TouchEndEvent");
onTouchEndedEvent.bind(TouchEnded);

// Update Event
function UpdateEvent(eventData)
{
	
}
var onUpdateEvent = script.createEvent("UpdateEvent")
onUpdateEvent.bind(UpdateEvent);

function radians_to_degrees(radians)
{
  var pi = Math.PI;
  return radians * (180/pi);
}