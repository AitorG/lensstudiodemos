// -----JS CODE-----

var sweater = script.getSceneObject();
var sweater_init_pos = sweater.getTransform().getWorldPosition();
var sweater_init_rot = sweater.getTransform().getWorldRotation();

var first = true;

var event = script.createEvent("LateUpdateEvent");
event.bind(function (eventData)
{
	
	
	
	
	if(first)
	{
		sweater_init_pos = sweater.getTransform().getWorldPosition();
		sweater_init_pos.y -= 0.1;
		sweater_init_rot = sweater.getTransform().getWorldRotation();
		first = false;
	}
	else
	{
		var pos = new vec3(sweater_init_pos.x, sweater_init_pos.y, sweater.getTransform().getWorldPosition().z);
		sweater.getTransform().setWorldPosition(pos);
		sweater.getTransform().setWorldRotation(sweater_init_rot);
		//print(sweater.getTransform().getWorldPosition().x +" " +sweater.getTransform().getWorldPosition().y +" "+sweater.getTransform().getWorldPosition().z);
	}
});