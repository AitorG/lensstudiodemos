//@input Component.SpriteVisual movieSprite

//@input Asset.Texture movieOpen
//@input Asset.Texture movieClose

var videoSource = script.movieSprite.getMaterial(0).getPass(0).baseTex;

global.logToScreen("caca");

function onStatus(eventData)
{
	var statu = script.movieSprite.getMaterial(0).getPass(0).baseTex.control.getStatus();
    global.logToScreen(statu);
};

function onTap(eventData)
{
	global.logToScreen("On Tap Status: " + videoSource.control.getStatus());
	if(videoSource.control.getStatus() === 0)
		videoSource.control.play(1);
	else
		videoSource.control.resume();
	videoSource.control.setOnFinish(prepareVideo);
	
}
var tapevent = script.createEvent("TapEvent");
tapevent.bind(onTap);


function prepareVideo()
{
	global.logToScreen("Preparing Video: " + videoSource.control.getStatus());
	if(videoSource.control.getStatus() === 0)
	{
		
		//global.logToScreen("Pre pause, status: " + videoSource.control.getStatus());
		//videoSource.control.pause();
		//global.logToScreen("Post pause, status: " + videoSource.control.getStatus());
		//global.logToScreen("Pre stop, status: " + videoSource.control.getStatus());
		//videoSource.control.stop();
		//global.logToScreen("Post stop, status: " + videoSource.control.getStatus());
		//global.logToScreen("Pre play, status: " + videoSource.control.getStatus());		
		//videoSource.control.play(1);
		
	}	
	
	(videoSource === script.movieOpen) ? videoSource = script.movieClose : videoSource = script.movieOpen;
	
	var count = script.movieSprite.getMaterial(0).getPass(0).baseTex.control.getCurrentPlayCount();
	global.logToScreen("The current count is: "+count);
}

global.logToScreen("caca2");
