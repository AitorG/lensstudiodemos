// TeamCelebrateController.js
// Version: 0.0.1
// Event: Lens Turned On
// Description: The primary script that drives the team celebrate template. Has
// a large assortment of exposed inputs and the logic to actually modify the 
// template content based on these inputs

//@input vec4 primaryColor = {1, 1, 1, 1} {"widget":"color"}
//@input vec4 secondaryColor = {1, 1, 1, 1} {"widget":"color"}
//@input bool useTertiaryColor = false
//@input vec4 tertiaryColor = {1, 1, 1, 1} {"showIf":"useTertiaryColor", "widget":"color"}

//@ui {"widget":"group_start", "label":"Confetti"}
//@input bool showConfetti = false
//@input float confettiIntensity = 0.5 {"showIf":"showConfetti", "widget":"slider", "min":0.0, "max":1.0, "step":0.01}
//@input bool customColorConfetti = false {"showIf":"showConfetti"}
//@input vec4 primaryConfettiColor = {1, 1, 1, 1} {"showIf":"customColorConfetti", "widget":"color"}
//@input vec4 secondaryConfettiColor = {1, 1, 1, 1} {"showIf":"customColorConfetti", "widget":"color"}
//@ui {"widget":"group_end"}

//@ui {"widget":"group_start", "label":"DO NOT EDIT", "showIf": "hideMe", "showIfValue": true}
//@input bool hideMe = false {"showIf": "hideMe"}
//@input Component.ScriptComponent properties
//@ui {"widget":"group_end"}

function init()
{
	configureConfetti();
	configureAudio();
}

script.api.initConfetti = init;

function configureConfetti()
{
	if( script.properties.api.confetti )
	{
		// Set Confetti Enabled
		script.properties.api.confetti.enabled = true;

		// Use default or custom colors
		var primaryConfettiColor = script.primaryColor;
		var secondaryConfettiColor = script.secondaryColor;
		var tertiaryConfettiColor = script.tertiaryColor;
		if( script.customColorConfetti )
		{
			primaryConfettiColor = script.primaryConfettiColor;
			secondaryConfettiColor = script.secondaryConfettiColor;
		}

		// Set Primary Confetti Color
		if(script.properties.api.confettiPrimaryMaterial)
		{
			primaryConfettiColor.a = 1;
			script.properties.api.confettiPrimaryMaterial.mainPass.colorMin = new vec3( primaryConfettiColor.r, primaryConfettiColor.g, primaryConfettiColor.b );
			script.properties.api.confettiPrimaryMaterial.mainPass.colorMax = new vec3( primaryConfettiColor.r, primaryConfettiColor.g, primaryConfettiColor.b );

			script.properties.api.confettiPrimaryMaterial.mainPass.spawnMaxParticles = lerp(10.0, 600.0, script.confettiIntensity);
		}

		// Set Secondary Confetti Color
		if(script.properties.api.confettiSecondaryMaterial)
		{
			secondaryConfettiColor.a = 1;
			script.properties.api.confettiSecondaryMaterial.mainPass.colorMin = new vec3( secondaryConfettiColor.r, secondaryConfettiColor.g, secondaryConfettiColor.b );
			script.properties.api.confettiSecondaryMaterial.mainPass.colorMax = new vec3( secondaryConfettiColor.r, secondaryConfettiColor.g, secondaryConfettiColor.b );

			script.properties.api.confettiSecondaryMaterial.mainPass.spawnMaxParticles = lerp(10.0, 600.0, script.confettiIntensity);
		}

		// Set Tertiary Confetti Color
		if( script.properties.api.tertiaryConfetti )
		{
			if(script.useTertiaryColor && script.properties.api.confettiTertiaryMaterial)
			{
				tertiaryConfettiColor.a = 1;
				script.properties.api.tertiaryConfetti.enabled = true;
				script.properties.api.confettiTertiaryMaterial.mainPass.colorMin = new vec3( tertiaryConfettiColor.r, tertiaryConfettiColor.g, tertiaryConfettiColor.b );
				script.properties.api.confettiTertiaryMaterial.mainPass.colorMax = new vec3( tertiaryConfettiColor.r, tertiaryConfettiColor.g, tertiaryConfettiColor.b );
	
				script.properties.api.confettiTertiaryMaterial.mainPass.spawnMaxParticles = lerp(10.0, 600.0, script.confettiIntensity);
			}
			else
			{
				script.properties.api.tertiaryConfetti.enabled = false;
			}
		}
	}
}

function getColorWithAlpha( color, alpha )
{
	return new vec4( color.x, color.y, color.z, alpha )
}

function lerp(a, b, t)
{
    return a * (1.0 - t) + b * t;
}