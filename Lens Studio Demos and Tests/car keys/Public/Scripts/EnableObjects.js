// -----JS CODE-----
/*
function onBackCamEvent(eventData)
{
    script.sceneObject.enabled = true;
    print("1");
}
var cameraBackEvent =
script.createEvent("CameraBackEvent");
cameraBackEvent.bind(onBackCamEvent);

function onFrontCamEvent(eventData)
{
    script.sceneObject.enabled = false;
    print("2");   
}
var cameraFrontEvent = script.createEvent("CameraFrontEvent");
cameraFrontEvent.bind(onFrontCamEvent);*/


global.userContextSystem.requestAltitudeFormatted(function(altitude) {
   global.logToScreen("Your altitude is: " + altitude);
});

global.userContextSystem.requestAltitudeInMeters(function(meters) {
   global.logToScreen("Your altitude in meters is: " + meters);
});

global.userContextSystem.requestBirthdate(function(birthdate) {
   global.logToScreen("Your birthday is: " + birthdate);
});

global.userContextSystem.requestBirthdateFormatted(function(birthdate) {
   global.logToScreen("Your birthday is: " + birthdate);
});

global.userContextSystem.requestCity(function(city) {
   global.logToScreen("Your city is: " + city);
});

global.userContextSystem.requestDisplayName(function(displayName) {
   global.logToScreen("Hello " + displayName);
});

global.userContextSystem.requestTemperatureCelsius(function(celsius) {
   global.logToScreen("The temperature in celsius is: " + celsius);
});

global.userContextSystem.requestTemperatureFahrenheit(function(fahrenheit) {
   global.logToScreen("The temperature in fahrenheit is: " + fahrenheit);
});

global.userContextSystem.requestTemperatureFormatted(function(temperature) {
   global.logToScreen("The temperature formated is: " + temperature);
});

global.userContextSystem.requestWeatherCondition(function(weatherconditionobject) {
   global.logToScreen("The weather condition outside is: " + weatherconditionobject);
});

global.userContextSystem.requestWeatherLocalized(function(weatherText) {
    global.logToScreen("The weather outside is: " + weatherText);
});

