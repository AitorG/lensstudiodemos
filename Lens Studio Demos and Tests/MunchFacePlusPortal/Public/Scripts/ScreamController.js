// -----JS CODE-----

// @input bool advanced = false
// @input SceneObject[] faceContent {"showIf": "advanced"}
// @input SceneObject[] worldContent {"showIf": "advanced"}

script.getSceneObject().getChild(0).enabled = false;
var hintComponent = script.getSceneObject().createComponent( "Component.HintsComponent" );

var HasMouthOpened = false;

var event = script.createEvent("MouthOpenedEvent");
event.bind(function (eventData)
{
	script.getSceneObject().getChild(0).enabled = true;
	HasMouthOpened = true;
	hintComponent.showHint("lens_hint_swap_camera", 2);
});
/*
var event = script.createEvent("MouthClosedEvent");
event.bind(function (eventData)
{
	script.getSceneObject().getChild(0).enabled = false;	
});*/
/*
var event = script.createEvent("FaceLostEvent");
event.bind(function (eventData)
{
	hintComponent.showHint("lens_hint_find_face", 3);
	script.getSceneObject().getChild(0).enabled = false;
});
*/

var event = script.createEvent("FaceFoundEvent");
event.bind(function (eventData)
{
	if(global.scene.getCameraType() == "front")
		if(HasMouthOpened === false)hintComponent.showHint("lens_hint_open_your_mouth", 2);
});
/*
var event = script.createEvent("CameraBackEvent");
event.bind(function (eventData)
{
	script.EffectObject.enabled = false;
	
	if(HasMouthOpened === true)
	{
		hintComponent.showHint("lens_hint_tap_ground", -1);
	}
	else
	{
		hintComponent.showHint("lens_hint_swap_camera", 3);		
	}
});
*/

function onFrontCamEvent(eventData)
{
    for(var i = 0; i < script.faceContent.length; i++)
    {
        var faceObject = script.faceContent[i];
        if(faceObject)
        {
            faceObject.enabled = true;
        }
    }  
    
    for(var i = 0; i < script.worldContent.length; i++)
    {
        var worldObject = script.worldContent[i];
        if(worldObject)
        {
            worldObject.enabled = false;
        }
    } 
}
var cameraFrontEvent = script.createEvent("CameraFrontEvent");
cameraFrontEvent.bind(onFrontCamEvent);

function onBackCamEvent(eventData)
{
    for(var i = 0; i < script.faceContent.length; i++)
    {
        var faceObject = script.faceContent[i];
        if(faceObject)
        {
            faceObject.enabled = false;
        }
    }  
    
    for(var i = 0; i < script.worldContent.length; i++)
    {
        var worldObject = script.worldContent[i];
        if(worldObject)
        {
            worldObject.enabled = true;
        }
    }  
}
var cameraBackEvent = script.createEvent("CameraBackEvent");
cameraBackEvent.bind(onBackCamEvent);


