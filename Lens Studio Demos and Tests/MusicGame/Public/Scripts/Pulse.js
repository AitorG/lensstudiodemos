// -----JS CODE-----

//@input float maxSize = 0.78 {"label":"MaxSize", "min":0.001, "max":2.0, "step":0.001}
//@input float minSize = 0.45 {"label":"MinSize", "min":0.001, "max":2.0, "step":0.001}

//@input float increment = 0.01

script.getSceneObject().getFirstComponent("Component.SpriteAligner").size = new vec2(script.increment,script.increment).add(script.getSceneObject().getFirstComponent("Component.SpriteAligner").size);

if(script.getSceneObject().getFirstComponent("Component.SpriteAligner").size.x > script.maxSize || script.getSceneObject().getFirstComponent("Component.SpriteAligner").size.y < script.minSize)
	script.increment *=-1;