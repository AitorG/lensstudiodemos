// -----JS CODE-----

//@input SceneObject Effects
//@input SceneObject Cat
//@input SceneObject UI

// set the variables
var gamestage = 0;
var comboCount = 0;
for(var i = 0; i < script.getSceneObject().getComponentCount("Component.AudioComponent"); i++)
{
	script.getSceneObject().getComponentByIndex("Component.AudioComponent", i).enabled = false;
}


function onUpdateEvent()
{
	//global.logToScreen("|m|");
}
script.api.updateEvent = script.createEvent("UpdateEvent");
script.api.updateEvent.bind(onUpdateEvent);

function onBrowsLoweredEvent()
{
	if(gamestage ==0)
		return;
		
	global.logToScreen("Brows Lowered");
	playSound(script.getSceneObject().getComponentByIndex("Component.AudioComponent",0));
}
script.api.browsLoweredEvent = script.createEvent("BrowsLoweredEvent");
script.api.browsLoweredEvent.bind(onBrowsLoweredEvent);

function onBrowsRaisedEvent()
{
	if(gamestage ==0)
		return;
	
	global.logToScreen("Brows Raised");
	playSound(script.getSceneObject().getComponentByIndex("Component.AudioComponent",1));
}
script.api.browsRaisedEvent = script.createEvent("BrowsRaisedEvent");
script.api.browsRaisedEvent.bind(onBrowsRaisedEvent);

function onBrowsReturnedToNormalEvent()
{
	if(gamestage ==0)
		return;
	
	global.logToScreen("Brows ReturnedToNormal");
}
script.api.browsReturnedToNormalEvent = script.createEvent("BrowsReturnedToNormalEvent");
script.api.browsReturnedToNormalEvent.bind(onBrowsReturnedToNormalEvent);

function onKissStartedEvent()
{
	if(gamestage ==0)
		return;
	
	global.logToScreen("Kiss Started");
}
script.api.kissStartedEvent = script.createEvent("KissStartedEvent");
script.api.kissStartedEvent.bind(onKissStartedEvent);

function onKissFinishedEvent()
{
	if(gamestage ==0)
		return;
	
	global.logToScreen("Kiss Finished");
}
script.api.kissFinishedEvent = script.createEvent("KissFinishedEvent");
script.api.kissFinishedEvent.bind(onKissFinishedEvent);

function onMouthClosedEvent()
{
	if(gamestage ==0)
		return;
	
	global.logToScreen("Mouth Closed");
}
script.api.mouthClosedEvent = script.createEvent("MouthClosedEvent");
script.api.mouthClosedEvent.bind(onMouthClosedEvent);

function onMouthOpenedEvent()
{
	if(gamestage ==0)
		return;
	
	global.logToScreen("Mouth Opened");
}
script.api.mouthOpenedEvent = script.createEvent("MouthOpenedEvent");
script.api.mouthOpenedEvent.bind(onMouthOpenedEvent);

function onSmileStartedEvent()
{
	if(gamestage == 0)
	{
		script.Effects.enabled = false;
		script.Cat.enabled = true;
		script.UI.enabled = true;
		InitializeAudioComponents();
		gamestage = 1;
	}
	global.logToScreen("Smile Started");
}
script.api.smileStartedEvent = script.createEvent("SmileStartedEvent");
script.api.smileStartedEvent.bind(onSmileStartedEvent);

function onSmileFinishedEvent()
{
	if(gamestage ==0)
		return;
	
	global.logToScreen("Smile Finished");
}
script.api.smileFinishedEvent = script.createEvent("SmileFinishedEvent");
script.api.smileFinishedEvent.bind(onSmileFinishedEvent);

function onComboDecreaseEvent()
{
	comboCount--;
}
script.api.comboDecreaseEvent = script.createEvent("DelayedCallbackEvent");
script.api.comboDecreaseEvent.bind(onComboDecreaseEvent);

function onComboEraseEvent()
{
	comboCount = 0;
}
script.api.comboEraseEvent = script.createEvent("DelayedCallbackEvent");
script.api.comboEraseEvent.bind(onComboEraseEvent);


function InitializeAudioComponents()
{
	for(var i = 0; i < script.getSceneObject().getComponentCount("Component.AudioComponent"); i++)
	{
		script.getSceneObject().getComponentByIndex("Component.AudioComponent", i).enabled = true;
	}
	
	global.logToScreen("Enabled Audio Components");
}

function PlayBongoCatAnimation( mode, fn )
{
	// Make the bongo cat appear on screen ->{FadeIn, Pass(startpoint, endpoint),}
	// Move left hand, right hand or both
	// Play Sound
	// Make the bongo cat dissapear from screen
}

function InterpolatePosition( start, end, t )
{
	
}

function playSound( component )
{
	component.play(1);
	comboCount++;
}