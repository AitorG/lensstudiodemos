// -----JS CODE-----

//@input Component.Label labelBillboard0
//@input Component.Label labelBillboard1
//@input Component.Label labelBillboard2
//@input Component.Label labelBillboard3
//@input Component.Label labelBillboard4
//@input Component.Label labelBillboard5
//@input Component.Label labelBillboard6
//@input Component.Label labelBillboard7
//@input Component.Label labelBillboard8
//@input Component.Label labelBillboard9
//@input Component.Label labelBillboard10
//@input Component.Label labelBillboard11
//@input Component.Label labelBillboard12

//@input Component.Camera mcamera
//@input SceneObject target
//@input SceneObject berry
//@input bool debugMode = false


if(script.debugMode)
{
    
script.labelBillboard0.text ="CP: "+ script.mcamera.getTransform().getWorldPosition().x;
script.labelBillboard1.text =""+ script.mcamera.getTransform().getWorldPosition().y;
script.labelBillboard2.text =""+ script.mcamera.getTransform().getWorldPosition().z;

script.labelBillboard3.text ="CR: "+ script.mcamera.getTransform().getWorldRotation().w;
script.labelBillboard4.text =""+ script.mcamera.getTransform().getWorldRotation().x;
script.labelBillboard5.text =""+ script.mcamera.getTransform().getWorldRotation().y;
script.labelBillboard6.text =""+ script.mcamera.getTransform().getWorldRotation().z;

script.labelBillboard10.text ="TP: "+ script.target.getTransform().getWorldPosition().x;
script.labelBillboard11.text =""+ script.target.getTransform().getWorldPosition().y;
script.labelBillboard12.text =""+ script.target.getTransform().getWorldPosition().z;

script.labelBillboard7.text ="BP: "+ script.berry.getTransform().getWorldPosition().x;
script.labelBillboard8.text =""+ script.berry.getTransform().getWorldPosition().y;
script.labelBillboard9.text =""+ script.berry.getTransform().getWorldPosition().z;
}