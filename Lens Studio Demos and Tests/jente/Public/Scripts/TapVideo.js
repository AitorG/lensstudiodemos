// -----JS CODE-----

//@input SceneObject VideoAssets
//@input SceneObject SpriteAssets

//@input Component.SpriteVisual movieSprite

script.VideoAssets.getChild(0).enabled = true;
//script.VideoAssets.getChild(0).getFirstComponent("Component.SpriteVisual").
for(var i = 1; i < script.VideoAssets.getChildrenCount(); i++)
{
	if(script.VideoAssets.getChild(i).getFirstComponent("Component.SpriteVisual").getMaterial(0).getPass(0).baseTex.name === script.movieSprite.getMaterial(0).getPass(0).baseTex.name)
	{
		script.VideoAssets.getChild(i).enabled = true;
		break;
	}
}
script.SpriteAssets.enabled = false;

script.movieSprite.getMaterial(0).getPass(0).baseTex.control.play(1);
script.movieSprite.getMaterial(0).getPass(0).baseTex.control.setOnFinish( revertChanges)

function revertChanges()
{
	script.VideoAssets.getChild(0).enabled = false;
	for(var i = 1; i < script.VideoAssets.getChildrenCount(); i++)
	{
		if(script.VideoAssets.getChild(i).getFirstComponent("Component.SpriteVisual").getMaterial(0).getPass(0).baseTex.name === script.movieSprite.getMaterial(0).getPass(0).baseTex.name)
		{
			script.VideoAssets.getChild(i).enabled = false;
			break;
		}
	}
	script.SpriteAssets.enabled = true;
}