// -----JS CODE-----
/********************************************************************/
/*!
\file   StrawberryController.js
\author Aitor Gandia
\par    Visual Engineering
\date   Feb 2019

\brief
Initialization, event handling and physics update of the fruits

Copyright (C) 2018 Visual Engineering AS.
Reproduction or disclosure of this file or its contents
without the prior written consent of Visual Engineering AS is prohibited.
/*********************************************************************/

// Main Camera
//@input Component.Camera mcamera

// Target
//@input SceneObject target

//@input Asset.ObjectPrefab prefab

//Billboard
//@input Component.Label labelBillboard0
//@input Component.SpriteVisual bannerBillboard

// Error allowed to consider fruit InPlace
//@input float DeltaInitialLengthError = 0.3f {"widget":"slider", "min":0.0, "max":20.0, "step":0.1}

// Rotation speed
//@input float RotationSpeed = 50 {"widget":"slider", "min":0.0, "max":90.0, "step":0.1}
//@input float minYValueToReset = -15.0 {"widget":"slider", "min":-100.0, "max":0.0, "step":0.1}

//@input Asset.Texture CapTexture
//@input Asset.Texture BodyTexture
//@input Asset.Texture StickerTexture

//@input bool mDebugMode = false

//@input Component.ScriptComponent properties
//@input SceneObject audioEmitter
//@input SceneObject instructionGroup

//@input Component.ScriptComponent GSM

// this fruit's info
var transform;
var objectClones = []; var cloneCount = 0;

// Position where the fruit will be placed again on TouchEnd (if:isFruitInPlace)
var initialPosition;

// yoghurt transform info
var targetTransform, targetWPos, targetWRot;
var targetinitialPosition;

// camera info references
var mycamera; var mycameraTransform; var cameramtx ;

// amout of degrees to rotate per frame
var degrees;

// Determine the status of the fruit using these booleans
var bTrackingFinger  = false;
var bFruitInPlace = true;
var bFruitFlying = false;

// Score System
var score = 0; var missCount = 0;
var label0, label1;

// distance of the camera to the fruit, used for the 2D screen coord to 3D world coord transformation
var depth;

// Track of fruit's velocity via position
var lastFramePos;
var currentFramePos;

// velocity of the fruit and dt related values for the physics update
var velocity = 0.0; 
var elapsedTime = 0.0;
var timeOnRelease = 0.0;

// Visual effects triggered on collision
var myAnimation;
var BbigObjMaterial;
var CapObjMaterial;
var BsmallObjMaterial;
var StickerObjMaterial;

InitializeAll();

function InitializeAll()
{
	//debugPrint("Initialize Fruit Logic");
	//script.audioEmitter.getFirstComponent("Component.ScriptComponent").api.PlayBGMusic();
	
	inputName = script.getSceneObject().name;
	
	transform = script.getTransform();
	
	targetTransform = script.target.getTransform();
	
	mycamera = script.mcamera;
	mycameraTransform = mycamera.getTransform();
	
	//script.getSceneObject().getTransform().setWorldPosition(mycameraTransform.getWorldPosition());
	initialPosition = script.getSceneObject().getTransform().getWorldPosition();
	targetinitialPosition = script.target.getTransform().getWorldPosition();
	
	degrees = script.RotationSpeed * getDeltaTime();
	
	
	
	depth = mycameraTransform.getWorldPosition().z - initialPosition.z;
	
	lastFramePos = initialPosition;
	currentFramePos = new vec3(0,0,0);
	
	//yoghurt explosion animation and material references.
	{
		// grab animation sprite and make it invisble on init, we will enable and play it on collision		
		myAnimation = script.target.getChild(4).getFirstComponent("Component.SpriteVisual");		 
		myAnimation.enabled = false;
		
		BbigObjMaterial = script.target.getChild(0).getFirstComponent("Component.MeshVisual").getMaterial(0);
		CapObjMaterial = script.target.getChild(1).getFirstComponent("Component.MeshVisual").getMaterial(0);
		BsmallObjMaterial = script.target.getChild(2).getFirstComponent("Component.MeshVisual").getMaterial(0);
		StickerObjMaterial = script.target.getChild(3).getFirstComponent("Component.MeshVisual").getMaterial(0);
	}
	//Billboard Label with score info
	{
		label0 = script.labelBillboard0;
		label0.text = "0 Points";
		label1 = script.bannerBillboard;
		label1.enabled = false;
	}
}

script.api.initialize = InitializeAll;

// Simple collision text assuming two bounding spheres of radious r
function checkCollisionFruitVsBV(fruit, target)
{
	var position = target.getWorldPosition();	
	var fruit_position = fruit.getWorldPosition();	
	var distance = Math.abs( position.distance(fruit_position));
	
	if(distance < 8 )	/*TODO: Remove magic number*/
	{			
		debugPrint("Collision");
		
		updateScore();
		
		playCollisionAnimation();
		
		UpdateYoghurtMaterials();		
		
		transform.setWorldPosition(initialPosition);			
		bFruitFlying = false;
	}
}

function onAnimFinished()
{
	myAnimation.enabled = false;
}

// Rotate a SceneObject around the world Y axis
var event = script.createEvent("UpdateEvent");
event.bind(function (eventData)
{
	// We only care about physics update when the fruit is free flying/falling
	if(bTrackingFinger == true)
        return;
	
    if(bFruitFlying == true)
	{
		//print("Pos: "+ transform.getWorldPosition().x+" "+ transform.getWorldPosition().y+" "+ transform.getWorldPosition().z);
		//print(targetTransform.getWorldPosition().y)
		// Do a parabolic throw y = y0 + v0*t - 0.5*a*t*t considering all the objects are converted to world coordinates
		var timeDiff = getTime() - timeOnRelease;
		
		var updatedPosition_x = currentFramePos.x + velocity.x * timeDiff;
		var updatedPosition_y = currentFramePos.y + velocity.y * timeDiff - 9.8*timeDiff*timeDiff;
		var updatedPosition_z = currentFramePos.z + velocity.z * timeDiff;
		
		transform.setWorldPosition(new vec3(updatedPosition_x, updatedPosition_y, updatedPosition_z));
		
		checkCollisionFruitVsBV(transform, targetTransform);
		
		// test both objects relative positions in the world and handle
		//if(cameramtx.multiplyPoint(transform.getWorldPosition()).z <= targetTransform.getWorldPosition().z)
		//{
		if(transform.getWorldPosition().y < targetTransform.getWorldPosition().y)
		{
			bFruitFlying = false;
			
			//var mytext = label2.text;
			//var to_number = parseInt(mytext.substr(0));
			//label1.enabled = true;
			//label2.text = (to_number+1)+"";
			
			//if(to_number + 1 >= 3)
			//{
			//	//label1.text = "You are not Berry good.";				
			//	//label2.text = "0";
			//}
			
			// clone object or recicle if there are enough
			//if(cloneCount < 4)
			//	objectClones[cloneCount] = script.prefab.instantiate(null);
			//else
			//	cloneCount = 0;
			//
			//objectClones[cloneCount].getTransform().setWorldPosition(transform.getWorldPosition())
			//cloneCount++;
			transform.setWorldPosition(initialPosition);	
		}
	}
	
    // Degrees to rotate the fruits by
    rotateBy( degrees );
});

// Start Tracking fruits
var event = script.createEvent("TouchStartEvent");
event.bind(function (eventData)
{
	//early rejection
	if(bTrackingFinger == true)
		return;
		
	if(bFruitFlying == true)
		return;
	
	if(label1.enabled === true)
		label1.enabled = false;	
	
	if(script.GSM.api.phaseThrowingBerries === true)
		script.GSM.api.disableInstructionPhase(2);
	//if(label2.enabled === true)
		//label2.enabled = false;
	
	debugPrint("touch start");
	
	bTrackingFinger = true;
	canEarnPoints= true;
	currentFramePos = transform.getWorldPosition();
});

// Allow object's rotation to update again, set conditions for physics update
var event = script.createEvent("TouchEndEvent");
event.bind(function (eventData)
{
	// early rejection
	if(bFruitFlying == true)
		return;
	
    debugPrint("touch ended");
	
    if(isFruitInPlace() || velocity.length < 0.1)
    {
        // reset object rotation to 0 (identity)
        transform.setWorldRotation(quat.quatIdentity());
		transform.setWorldPosition(initialPosition);
        bTrackingFinger = false;
    }
    else //if (isFruitFlying)
    {	
		// treat the converted velocity vector to normalize values.
		velocity = new vec3(clamp(velocity.x, -2, 2), clamp(velocity.y, -20, 20), clamp(velocity.z * 2, -25, 25));
		timeOnRelease = getTime();
		bTrackingFinger = false;
		bFruitFlying = true;
		script.audioEmitter.getFirstComponent("Component.ScriptComponent").api.PlayThrowBerrySound();
    }
});

// Make the fruit follow finger until release
var event = script.createEvent("TouchMoveEvent");
event.bind(function (eventData)
{
	// early rejection
	if(bFruitFlying == true)
		return;
	
	var touchpos = eventData.getTouchPosition();	
	var world_coord_touch_pos = mycamera.screenSpaceToWorldSpace(eventData.getTouchPosition(), Math.abs(depth));
	transform.setWorldPosition(world_coord_touch_pos);
	
	// update  position
	lastFramePos = currentFramePos;
	currentFramePos = transform.getWorldPosition();
	
	//transformWPos = transform.getWorldPosition();
	
	// compute velocity of the fruit
	var diff = currentFramePos.sub(lastFramePos);	
	velocity = diff.div(new vec3(getDeltaTime(),getDeltaTime(), getDeltaTime()));
	
	//not interested in going toward these directions (camera forward aligned with world negative Z-axis)
	if(velocity.y <= 0 || velocity.z >= 0)
		velocity = new vec3(0,0,0);
});

// Check for minimmum world space movement to launch the berry. 
function isFruitInPlace()
{
    var distance = Math.abs((transform.getWorldPosition()).distance(initialPosition));    
    return (distance <= script.DeltaInitialLengthError)? true : false;	
}

// Align coordinate systems when the tracking is active
function rotateVectorsToWorld(_axis, _vector)
{
	// rotation of the world
	var rot = quat.rotationFromTo(mycameraTransform.forward, targetTransform.forward);		
	// Degrees to rotate by, counteract the tracker rotation to align axis
	var degrees = -90 * getDeltaTime();	
	// Convert degrees to radians
	var radians = degrees * (Math.PI / 180);	
	// Axis to rotate around
	var axis = _axis;
	// Rotation we will apply to the object's current rotation
	var rotationToApply = quat.angleAxis(radians, axis);
	// concatenate world rotation with tracker fixed rotation
	rot = rotationToApply.multiply(rot);
	// create rotation matrix
	cameramtx = mat4.compose(vec3.zero(), rot, vec3.one());
	// rotate the vector '_vector' by rotation matrix
	return cameramtx.multiplyDirection(_vector);
}

// Visual Berry rotation
function rotateBy( _degrees )
{
     // Convert degrees to radians
    var radians = _degrees * (Math.PI / 180);

    // Axis to rotate around
    var axis = vec3.up();

    // Rotation we will apply to the object's current rotation
    var rotationToApply = quat.angleAxis(radians, axis);
	
    // Get the object's current world rotation
    var oldRotation = transform.getWorldRotation();
	
    // Get the new rotation by rotating the old rotation by rotationToApply
    var newRotation = rotationToApply.multiply(oldRotation);
	
	// rotate berries every frame
	transform.setWorldRotation(newRotation);
	transform.setWorldRotation(newRotation);
	
	
}

// Math.extension, bound a number between min and max.
function clamp(number, min, max)
{
	return Math.min(Math.max(number, min), max);
}

// Helper debug funcions
function debugPrint(msg)
{
	if(script.mDebugMode === true)
		print(msg);
}

// Helper debug funcions
function printvec3(msg, x, y ,z)
{
	debugPrint(msg + x +" "+y+" "+z);
}

// Billboard user info
function updateScore()
{
	score = label0.text;
	var numberEndPos = score.indexOf(" ");
	var to_number = parseInt(score.substr(0, numberEndPos));
	var mytext = (to_number+50)+" points";
	label0.text = mytext;
	//label2.text = "0";
	
	if(to_number + 50 >= 200)
	{
		label1.enabled = true;
		script.properties.api.initConfetti();
		if(script.audioEmitter.getFirstComponent("Component.ScriptComponent").api.IsBGMusicPlaying())
		{
			script.audioEmitter.getFirstComponent("Component.ScriptComponent").api.pauseBGMusic();
		}
		script.audioEmitter.getFirstComponent("Component.ScriptComponent").api.PlayLevelWinSound();
		script.audioEmitter.getFirstComponent("Component.ScriptComponent").api.SetLevelWinSoundFinish();		
		label0.text = "0 points";
	}
	else
		script.audioEmitter.getFirstComponent("Component.ScriptComponent").api.PlayBerryHitSound();
}

// Handle Spritesheet animation, set and play. Reset state on finished
function playCollisionAnimation()
{
	myAnimation.enabled = true;			
	var loops = 1;
	var offset = 0.0;
	myAnimation.getMaterial(0).getPass(0).baseTex.control.play(loops, offset);
	myAnimation.getMaterial(0).getPass(0).baseTex.control.setOnFinish(onAnimFinished);	
}

// Handle material substitution.
function UpdateYoghurtMaterials()
{
	//debugPrint(material.getPass(0).baseTex.name);
	CapObjMaterial.getPass(0).baseTex = script.CapTexture;
	StickerObjMaterial.getPass(0).baseTex = script.StickerTexture;
	BsmallObjMaterial.getPass(0).baseTex = script.BodyTexture;
	BbigObjMaterial.getPass(0).baseTex = script.BodyTexture;
}