// -----JS CODE-----

// Play Audio
// @input Component.AudioComponent ThrowAway
// @input Component.AudioComponent WaterHit
// @input Component.AudioComponent LevelWin
// @input Component.AudioComponent BGMusic



script.api.PlayThrowBerrySound = ThrowBerry;
script.api.PlayBerryHitSound = BerryHit;
script.api.PlayLevelWinSound = LevelWin;
script.api.SetLevelWinSoundFinish = SetLevelWinFinish;
script.api.PlayBGMusic = BGMusic;
script.api.IsBGMusicPlaying = IsBGMusicPlaying;
script.api.pauseBGMusic = pauseBGMusic;
script.api.resumeBGMusic = resumeBGMusic;
script.api.stopBGMusic = stopBGMusic;

function ThrowBerry()
{
	script.ThrowAway.play( 1 );
}

function BerryHit()
{
	script.WaterHit.play(1);
	
}

function LevelWin()
{
	script.LevelWin.play(1);
	
}

BGMusic();

function BGMusic()
{
	script.BGMusic.play(-1);	
}

function IsBGMusicPlaying()
{
	return script.BGMusic.isPlaying();
}

function pauseBGMusic()
{
	script.BGMusic.pause();
}

function resumeBGMusic()
{
	script.BGMusic.resume();
}

function stopBGMusic(fade)
{
	script.BGMusic.stop(fade);
}

function SetLevelWinFinish()
{
	script.LevelWin.setOnFinish(resumeBGMusic);
}