// -----JS CODE-----

if(script.api.accumulate)
{
	script.api.time += global.getDeltaTime();
	if(script.api.time >= 17.0)
	{
		var provider = script.api.movieSprite.getMaterial(0).getPass(0).baseTex.control;
		global.logToScreen("Status: "+provider.getStatus());
		if(provider.getStatus() == VideoStatus.Paused)
		{
			global.logToScreen("Video is Paused.")
			return;
		}
		
		global.logToScreen("From Update Event: Time is > 17 and name " + script.api.movieSprite.getMaterial(0).getPass(0).baseTex.name);
		
		if(script.api.movieSprite && script.api.movieSprite.getMaterial(0).getPass(0).baseTex.control.getStatus() == VideoStatus.Playing)
		{
			script.api.movieSprite.getMaterial(0).getPass(0).baseTex.control.pause();
		
			script.accumulate = false;
			script.getSceneObject().createComponent("Component.HintsComponent").showHint("lens_hint_tap", 1);
		}
	}
}