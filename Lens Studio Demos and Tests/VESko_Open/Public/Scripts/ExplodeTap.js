// -----JS CODE-----

global.logToScreen("Tap")
if(script.api.movieSprite)
{
	global.logToScreen("Inside Tap")
	global.logToScreen("From Tap Event: " + script.api.movieSprite.getMaterial(0).getPass(0).baseTex.name);
	var provider = script.api.movieSprite.getMaterial(0).getPass(0).baseTex.control;
	
	if(!provider)
	{
		global.logToScreen("No provider found");
		return;
	}
	
	if(provider.getStatus() == VideoStatus.Playing)
	{
		global.logToScreen("Video is already Playing");
		return;
	}
	else if(provider.getStatus() == VideoStatus.Preparing)
	{
		global.logToScreen("Video is being prepared");
		return;
	}
	else if(provider.getStatus() == VideoStatus.Stopped)
	{
		global.logToScreen("Play Video");
		script.api.movieSprite.getMaterial(0).getPass(0).baseTex.control.play(1);
		script.api.accumulate = true;
	}
	else if(provider.getStatus() == VideoStatus.Paused)
	{
		global.logToScreen("Resume Video");
		script.api.movieSprite.getMaterial(0).getPass(0).baseTex.control.resume();
		script.api.accumulate = false;
	}
	else
	{
		global.logToScreen("Unknown video status error");
		return;
	}
}