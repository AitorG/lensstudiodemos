// -----JS CODE-----

script.api.time = 0;
script.api.accumulate = false;
script.api.movieSprite = script.getSceneObject().getFirstComponent("Component.SpriteVisual");

if(script.api.movieSprite)
{
	global.logToScreen("From Init: " + script.api.movieSprite.getMaterial(0).getPass(0).baseTex.name);
	script.api.movieSprite.getMaterial(0).getPass(0).baseTex.control.setOnFinish(function(){global.logToScreen("ResetTime");script.api.time = 0;})
}