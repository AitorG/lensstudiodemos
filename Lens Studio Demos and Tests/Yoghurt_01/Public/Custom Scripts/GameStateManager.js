// -----JS CODE-----

//@input SceneObject instructionGroupPhase2

script.instructionGroupPhase2.enabled = true;
script.api.phaseThrowingBerries  = true;


function DisableInstructionPhase(number)
{
	script.api.phaseThrowingBerries = false;		
	script.instructionGroupPhase2.enabled = false;
}
script.api.disableInstructionPhase = DisableInstructionPhase;
