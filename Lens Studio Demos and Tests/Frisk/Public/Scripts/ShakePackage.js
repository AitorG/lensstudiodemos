// -----JS CODE-----

//@input int direction
script.api.isTapped = false;
script.api.isMoving = false;

var fiftyDegQuat = new quat(0.906308,0,0,0.422618);
var twentyDegQuat = new quat(0.984808,0,0,0.173648);
var OrigQuat = new quat(1,0,0,0);

var targetRot = new quat(fiftyDegQuat.w,fiftyDegQuat.x, fiftyDegQuat.y, fiftyDegQuat.z  * script.direction);

var tolerance = 0.001;

function onTap( )
{
	//if it is already tapped on enter
	if(script.api.isTapped)
		return;
	
	script.api.isTapped = !script.api.isTapped;
	script.api.isMoving = !script.api.isMoving;
	
	if(script.api.isTapped)
		stopShakingEvent.reset(3);
}
var tapEvent = script.createEvent("TapEvent");
tapEvent.bind(onTap);


function onFrameUpdated( )
{
	if(script.api.isTapped && script.api.isMoving)
	{
		//current rotation of the candy sprite
		var rot = script.getSceneObject().getTransform().getWorldRotation();
		// check if close enough to target rotation
		if(Math.abs(rot.w - targetRot.w) <= tolerance)
		{
			// select new target rotation
			(targetRot.equal(new quat(fiftyDegQuat.w,fiftyDegQuat.x, fiftyDegQuat.y, fiftyDegQuat.z  * script.direction))) ? targetRot = twentyDegQuat : targetRot = fiftyDegQuat;
			// adapt the z direction for left and right candy sprites
			targetRot = new quat(targetRot.w, targetRot.x, targetRot.y, targetRot.z  * script.direction);
		}
		// interpolate the rotation
		var newrot = quat.slerp(rot, targetRot, 9*getDeltaTime());
		// set new rotation
		script.getSceneObject().getTransform().setWorldRotation(newrot);
	}
	else if(script.api.isTapped && !script.api.isMoving)
	{
		//current rotation of the candy sprite
		var rot = script.getSceneObject().getTransform().getWorldRotation();
		// check if close enough to target rotation
		if(rot.w - targetRot.w  <= tolerance)
		{
			script.api.isTapped = false;
			script.getSceneObject().getTransform().setWorldRotation(targetRot);
			//print(isTapped+" "+isMoving+" "+rot);
			return;
		}
		
		// interpolate the rotation
		var newrot = quat.slerp(rot, targetRot, 9*getDeltaTime());
		// set new rotation
		script.getSceneObject().getTransform().setWorldRotation(newrot);
	}
}
var updateEvent = script.createEvent("UpdateEvent");
updateEvent.bind(onFrameUpdated);


function onStopShakingEvent(eventData)
{
	script.api.isMoving = false;
	targetRot = OrigQuat;
}
var stopShakingEvent = script.createEvent("DelayedCallbackEvent")
stopShakingEvent.bind(onStopShakingEvent);

function onToggleTaps(eventData)
{
	tapEvent.enabled = script.api.toggleTapsEvent.data.enabled;
}
script.api.toggleTapsEvent = script.createEvent("DelayedCallbackEvent")
script.api.toggleTapsEvent.bind(onToggleTaps);
