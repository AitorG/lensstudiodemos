// -----JS CODE-----

//@input float RotSpeed {"widget":"slider", "min":0.01, "max":1.0, "step":0.01}

var childrenCount = script.getSceneObject().getChildrenCount();
var rotations = [];

function createRandomRotation( )
{
	var x = Math.random();
	var y = Math.random();
	var z = Math.random();
	
	var v3 = new vec3(x,y,z).uniformScale(script.RotSpeed);
	//v3.normalize();
	return v3;
}

for(var i =0; i < childrenCount; i++)
{
	rotations[i] = createRandomRotation();
}

function onFrameUpdated( )
{
	if(script.getSceneObject().enabled)
	{
		for(var i =0; i < childrenCount; i++)
		{
			var startRot = script.getSceneObject().getChild(i).getTransform().getWorldRotation();			
			var newRot = startRot.multiply(quat.fromEulerAngles(rotations[i].x, rotations[i].y, rotations[i].z));
			
			script.getSceneObject().getChild(i).getTransform().setWorldRotation(newRot);
		}
	}
}
var updateEvent = script.createEvent("UpdateEvent");
updateEvent.bind(onFrameUpdated);